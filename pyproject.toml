[build-system]
requires = ["hatchling", "hatch-vcs"]
build-backend = "hatchling.build"

[project]
name = "galactic-core-closure"
dynamic = ["version"]
description = "GALACTIC closure package"
readme = "README.rst"
requires-python = ">=3.10,<3.14"
license = "BSD-3-Clause"
authors = [{name = "The Galactic Organization", email = "contact@thegalactic.org"}]
maintainers = [{name = "The Galactic Organization", email = "contact@thegalactic.org"}]
keywords = ["formal concept analysis", "closures", "Galois connection"]
classifiers = [
  # How mature is this project? Common values are
  # 1 - Planning
  # 2 - Pre-Alpha
  # 3 - Alpha
  # 4 - Beta
  # 5 - Production/Stable
  # 6 - Mature
  # 7 - Inactive
  "Development Status :: 4 - Beta",
  # Specify the OS
  "Operating System :: OS Independent",
  # Indicate who your project is intended for
  "Environment :: Console",
  "Intended Audience :: End Users/Desktop",
  "Intended Audience :: Developers",
  # Specify the Python versions you support here. In particular, ensure
  # that you indicate whether you support Python 2, Python 3 or both.
  "Programming Language :: Python :: 3.10",
  "Programming Language :: Python :: 3.11",
  "Programming Language :: Python :: 3.12",
  "Programming Language :: Python :: 3.13",
  # Natural language used
  "Natural Language :: English"
]
dependencies = [
  "galactic-helpers-core>=0.4.0.0.post1.dev",
  "galactic-core-kernel>=0.4.0.0.post1.dev",
  "compose~=1.6"
]

[project.urls]
Homepage = "https://galactic.univ-lr.fr/"
Documentation = "https://galactic.univ-lr.fr/docs/core/closure/"
Repository = "https://gitlab.univ-lr.fr/galactic/public/src/core/galactic-core-closure"
Issues = "https://gitlab.univ-lr.fr/galactic/public/src/core/galactic-core-closure/-/issues"
Changelog = "https://galactic.univ-lr.fr/docs/core/closure/latest/release-notes.html"

[project.optional-dependencies]
docs = [
  "galactic-core-closure.docs>=0.4.0.0.post1.dev",
  "graphviz~=0.20",
  "ipykernel~=6.29",
  "jupyter~=1.0"
]

[tool.hatch.envs.default.env-vars]
PIP_INDEX_URL = "https://{env:GITLAB_USERNAME:__token__}:{env:GITLAB_TOKEN:}@gitlab.univ-lr.fr/api/v4/groups/galactic/-/packages/pypi/simple/"
UV_INDEX_URL = "https://{env:GITLAB_USERNAME:__token__}:{env:GITLAB_TOKEN:}@gitlab.univ-lr.fr/api/v4/groups/galactic/-/packages/pypi/simple/"

[tool.hatch.envs.hatch-test]
extra-dependencies = [
  "pytest-custom_exit_code~=0.3"
]

[tool.hatch.envs.hatch-test.env-vars]
PIP_INDEX_URL = "https://{env:GITLAB_USERNAME:__token__}:{env:GITLAB_TOKEN:}@gitlab.univ-lr.fr/api/v4/groups/galactic/-/packages/pypi/simple/"
UV_INDEX_URL = "https://{env:GITLAB_USERNAME:__token__}:{env:GITLAB_TOKEN:}@gitlab.univ-lr.fr/api/v4/groups/galactic/-/packages/pypi/simple/"

[tool.hatch.envs.hatch-test.scripts]
run = "pytest{env:HATCH_TEST_ARGS:} {args}"
run-cov = "coverage run -m pytest{env:HATCH_TEST_ARGS:} {args}"
cov-combine = "coverage combine"
cov-report = [
  "coverage report",
  "coverage xml",
  "coverage html"
]

[[tool.hatch.envs.hatch-test.matrix]]
python = ["3.10", "3.11", "3.12", "3.13"]

[tool.hatch.envs.hatch-static-analysis]
dependencies = [
  # Types
  # Formatter
  "black[jupyter]~=25.1",
  # Style checkers
  "doc8~=1.1",
  "mypy~=1.15",
  "teyit~=0.4",
  "refurb~=2.0",
  "ruff~=0.9"
]

[tool.hatch.envs.hatch-static-analysis.scripts]
format-check = [
  "black --check --diff {args:src tests docs}",
  "doc8 -q {args:docs README.rst} -e .rst -e .md"
]
format-fix = [
  "black {args:src tests docs}"
]
lint-check = [
  "ruff check {args:src tests docs}",
  "refurb {args:src tests docs}",
  "mypy --package galactic",
  "teyit --show-stats --fail-on-change {args:tests}"
]
lint-fix = [
  "ruff check --fix {args:src tests docs}"
]

[tool.hatch.envs.lint]
dependencies = [
  "slotscheck~=0.19",
  "pylint~=3.3"
]

[tool.hatch.envs.lint.scripts]
check = [
  "slotscheck {args:src}",
  "pylint {args:src}"
]

[tool.hatch.envs.docs]
dependencies = [
  "galactic-core-closure[docs]",
  "myst-parser~=4.0",
  "Sphinx~=8.1",
  "sphinx-rtd-theme~=3.0",
  "sphinx-copybutton~=0.5",
  "nbsphinx~=0.9",
  "sphinxcontrib-plantuml~=0.30",
  "sphinx-autodoc-typehints~=3.0",
  "sphinx-codeautolink~=0.16",
  "jupyter~=1.0",
  "towncrier~=24.8"
]

[tool.hatch.envs.docs.scripts]
build = [
  "towncrier build --keep",
  "sphinx-build docs {args:build/sphinx/html}",
  "git checkout HEAD -- docs/release-notes.rst"
]
changelog = [
  "towncrier build {args:--yes --version `hatch version`}"
]

[tool.hatch.version]
source = "vcs"

[tool.hatch.version.raw-options]
version_scheme = "no-guess-dev"
local_scheme = "no-local-version"

[tool.hatch.build.targets.wheel]
packages = ["src/galactic"]

[tool.pytest.ini_options]
consider_namespace_packages = true
pythonpath = ["src"]

[tool.coverage.report]
exclude_lines = [
  "return NotImplemented",
  "raise NotImplementedError",
  "import doctest",
  "doctest.testmod()",
  "coverage: ignore"
]
exclude_also = [
  "def __repr__",
  "raise NotImplementedError",
  "if __name__ == .__main__.:",
  "if TYPE_CHECKING:",
  "class .*\\bProtocol\\):",
  "@(abc\\.)?abstractmethod"
]

[tool.coverage.run]
include = [
  "src/*"
]

[tool.coverage.html]
directory = "build/coverage/html"

[tool.coverage.xml]
output = "build/coverage.xml"

[tool.ruff]
# Allow lines to be as long as 88.
line-length = 88
target-version = "py310"

[tool.ruff.lint]
select = ["ALL"]
ignore = [
  # Variable is shadowing a Python builtin
  "A001",
  # Unused method argument
  "ARG002",
  # Function is too complex
  "C901",
  # One-line docstring should fit on one line
  "D200",
  # Exception must not use a string literal, assign to variable first
  "EM101",
  # Exception must not use an f-string literal, assign to variable first
  "EM102",
  # Boolean-typed positional argument in function definition
  "FBT001",
  # Boolean default positional argument in function definition
  "FBT002",
  # Boolean positional value in function call
  "FBT003",
  # Too many return statements
  "PLR0911",
  # Too many branches
  "PLR0912",
  # Too many arguments in function definition
  "PLR0913",
  # Magic value used in comparison, consider replacing with a constant variable
  "PLR2004",
  # `for` loop variable overwritten by assignment target
  "PLW2901",
  # `if` test must be a simple comparison against `sys.platform` or `sys.version_info`
  "PYI002",
  # Comment contains ambiguous
  "RUF003",
  # `__all__` is not sorted
  "RUF022",
  # `__slots__` is not sorted
  "RUF023",
  # Private member accessed
  "SLF001",
  # Move application import into a type-checking block
  "TC001",
  # Avoid specifying long messages outside the exception class
  "TRY003"
]

[tool.ruff.lint.pydocstyle]
convention = "numpy"

[tool.ruff.lint.per-file-ignores]
"tests/*" = [
  # Missing return type annotation for public function
  "ANN201",
  # Missing docstring in public class
  "D101",
  # Missing docstring in public method
  "D102",
  # Missing docstring in public package
  "D104",
  # File is part of an implicit namespace package. Add an `__init__.py`.
  "INP001",
  # Use a regular `assert` instead of unittest-style `assertEqual`
  "PT009",
  # Use `pytest.raises` instead of unittest-style `assertRaises`
  "PT027",
  # Name compared with itself
  "PLR0124",
  # Too many statements
  "PLR0915"
]
"docs/*" = [
  # File is part of an implicit namespace package. Add an `__init__.py`
  "INP001",
  # Variable name contains a non-ASCII character
  "PLC2401",
  # `print` found
  "T201"
]

[tool.refurb]
ignore = [139, 156]

[tool.mypy]
ignore_missing_imports = true
no_implicit_optional = true
# equivalent to --strict option
warn_unused_configs = true
disallow_any_generics = true
disallow_subclassing_any = true
disallow_untyped_calls = true
disallow_untyped_defs = true
disallow_incomplete_defs = true
check_untyped_defs = true
disallow_untyped_decorators = true
warn_redundant_casts = true
warn_unused_ignores = true
warn_return_any = true
no_implicit_reexport = true
strict_equality = true
extra_checks = true
mypy_path = "src"

[tool.pylint.main]
ignore-paths = [".*.pyi"]

[tool.pylint.'MESSAGES CONTROL']
disable = [
  "fixme",
  "similarities",
  "missing-module-docstring"
]

[tool.pylint.'FORMAT']
# Maximum number of characters on a single line.
max-line-length = 120

[tool.towncrier]
directory = "changes"
version = "|version|"
filename = "docs/release-notes.rst"
issue_format = "`#{issue} <https://gitlab.univ-lr.fr/galactic/public/src/core/galactic-core-closure/-/issues/{issue}>`_"

[[tool.towncrier.type]]
directory = "prelude"
name = "Prelude"
showcontent = true

[[tool.towncrier.type]]
directory = "feature"
name = "Features"
showcontent = true

[[tool.towncrier.type]]
directory = "issue"
name = "Known Issues"
showcontent = true

[[tool.towncrier.type]]
directory = "removal"
name = "Deprecations and Removals"
showcontent = true

[[tool.towncrier.type]]
directory = "critical"
name = "Critical Fixes"
showcontent = true

[[tool.towncrier.type]]
directory = "bugfix"
name = "Bug Fixes"
showcontent = true

[[tool.towncrier.type]]
directory = "security"
name = "Security Issues"
showcontent = true

[[tool.towncrier.type]]
directory = "misc"
name = "Other Notes"
showcontent = true
