""":mod:`galactic.algebras.connection` test module."""

from unittest import TestCase

from galactic.algebras.examples.connection import IntegerConnection


class ConnectionTest(TestCase):
    def test___init__(self):
        connection = IntegerConnection([2, 3, 4, 5, 6], [12, 18, 24, 36, 48, 30])
        self.assertEqual(connection.domain, {2, 3, 4, 5, 6})
        self.assertEqual(connection.co_domain, {12, 18, 24, 36, 48, 30})

    def test_multiple_polarity(self):
        connection = IntegerConnection([2, 3, 4, 5, 6], [12, 18, 24, 36, 48, 30, 60])
        self.assertEqual(
            set(connection.polarities[0]()),
            {12, 18, 24, 36, 48, 30, 60},
        )
        self.assertEqual(
            set(connection.polarities[0](elements=[2, 3, 4])),
            {12, 24, 36, 48, 60},
        )
        divisors_1 = connection.polarities[1](elements=[12])
        divisors_2 = connection.polarities[1](elements=[30])
        self.assertEqual(set(divisors_1), {2, 3, 4, 6})
        self.assertEqual(set(divisors_2), {2, 3, 5, 6})
        self.assertEqual(set(connection.polarities[0](divisors_1, divisors_2)), {60})

    def test_divisor_polarity(self):
        connection = IntegerConnection([2, 3, 4, 5, 6], [12, 18, 24, 36, 48, 30])
        self.assertEqual(
            set(connection.polarities[1](elements=[12, 24, 36, 48])),
            {2, 3, 4, 6},
        )
        multiple = connection.polarities[0](elements=[2, 3])
        self.assertEqual(set(connection.polarities[1](multiple)), {2, 3, 6})

    def test_closures(self):
        connection = IntegerConnection([2, 3, 4, 5, 6], [12, 18, 24, 36, 48, 30, 60])
        self.assertEqual(set(connection.closures[0](elements=[2, 3, 4])), {2, 3, 4, 6})
        self.assertEqual(
            set(connection.closures[1](elements=[12])),
            {12, 24, 36, 48, 60},
        )
