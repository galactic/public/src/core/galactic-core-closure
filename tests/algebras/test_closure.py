"""Closure tests."""

# This Python file uses the following encoding: utf-8

from unittest import TestCase

from galactic.algebras.examples.closure import (
    ExtensibleNumericalFamily,
    NumericalClosure,
)
from galactic.algebras.examples.connection import IntegerConnection


class NumericalClosureTestCase(TestCase):
    def test___init__(self):
        closure = NumericalClosure(0, 9)
        self.assertEqual(
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            list(closure()),
            "The universe of numerical_closure is [0,1,2,3,4,5,6,7,8,9]",
        )

    def test___call__(self):
        closure = NumericalClosure(0, 9)
        self.assertEqual(
            [2, 3, 4, 5, 6],
            list(closure(elements=[2, 3, 6])),
            "The NumericalClosure of [2, 3, 6] is [2, 3, 4, 5, 6]",
        )
        self.assertEqual(
            [],
            list(closure(elements=[])),
            "The NumericalClosure of [] is []",
        )


class NumericalClosedTestCase(TestCase):
    def test___init__(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        closed = closure(elements=[3, 7])
        self.assertEqual(
            list(closed),
            list(closure(elements=[3, 4, 7])),
            "The FamilyClosure of [3, 4, 7] is the closed of [3, 7]",
        )

    def test___le__(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        closed = closure(elements=[3, 7])
        self.assertLessEqual(
            closed,
            closure(elements=[3, 4, 7]),
            "The FamilyClosure of [3, 7] is <= to the closure of [3, 4, 7]",
        )
        self.assertLessEqual(
            closed,
            closure(elements=[3, 4, 7, 8]),
            "The FamilyClosure of [3, 7] is <= to the closure of [3, 4, 7, 8]",
        )
        self.assertFalse(
            closed <= closure(elements=[3, 4]),
            "The FamilyClosure of [3, 7] is not <= to the closure of [3, 4]",
        )

    def test___ge__(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        closed = closure(elements=[3, 7])
        self.assertGreaterEqual(
            closed,
            closure(elements=[3, 4, 7]),
            "The FamilyClosure of [3, 7] is >= to the closure of [3, 4, 7]",
        )
        self.assertGreaterEqual(
            closed,
            closure(elements=[3, 4]),
            "The FamilyClosure of [3, 7] is >= to the closure of [3, 4]",
        )
        self.assertFalse(
            closed >= closure(elements=[3, 8]),
            "The FamilyClosure of [3, 7] is not >= to the closure of [3, 8]",
        )

    def test_support(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        closed = closure(elements=[3, 7])
        self.assertEqual(closed.support, 0.5)
        closure = NumericalClosure()
        self.assertEqual(closure().support, 1.0)

    def test_equivalence(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        closed = closure(elements=[3, 7])
        self.assertEqual(
            list(closed.equivalence(3)),
            [3],
            "The equivalence of 3 is [3]",
        )

        self.assertEqual(
            list(closed.equivalence(8)),
            [],
            "The equivalence of 8 is [], 8 is not in the closed set [3..7]",
        )

        connection = IntegerConnection([2, 3, 4, 5, 6], [12, 18, 24, 36, 48, 30])
        closure = connection.closures[0]
        closed = closure(elements=[2, 6])
        self.assertEqual(
            list(closed.equivalence(2)),
            [2, 3, 6],
        )
        self.assertEqual(
            list(closed.equivalence(5)),
            [],
        )

    def test_subsumption(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        closed = closure(elements=[3, 7])
        self.assertEqual(
            list(closed.subsumption(1)),
            [],
            "The subsumption of 1 is []",
        )

        connection = IntegerConnection([2, 3, 4, 5, 6], [12, 18, 24, 36, 48, 30])
        closure = connection.closures[0]
        closed = closure(elements=[2, 6])
        self.assertEqual(
            list(closed.subsumption(2)),
            [],
        )
        self.assertEqual(
            list(closed.subsumption(5)),
            [2, 3, 6],
        )

    def test_supsumption(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        closed = closure(elements=[3, 7])
        self.assertEqual(
            list(closed.supsumption(3)),
            [],
            "The supsumption of 1 is []",
        )

        connection = IntegerConnection([2, 3, 4, 5, 6], [12, 18, 24, 36, 48, 30])
        closure = connection.closures[0]
        closed = closure(elements=[2, 6])
        self.assertEqual(
            list(closed.supsumption(2)),
            [],
        )
        self.assertEqual(
            list(closed.supsumption(5)),
            [],
        )


# noinspection PyTypeChecker
class NumericalFamilyTestCase(TestCase):
    def test___init__(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        family = ExtensibleNumericalFamily()
        family.extend([closure(elements=[1, 2, 3]), closure(elements=[2, 3, 4])])
        self.assertEqual(
            family,
            {
                closure(elements={1, 2, 3, 4}),
                closure(elements={1, 2, 3}),
                closure(elements={2, 3, 4}),
                closure(elements={2, 3}),
            },
        )

    def test_parts(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        family = ExtensibleNumericalFamily()
        family.extend([closure(elements=[1, 2, 3]), closure(elements=[2, 3, 4])])
        self.assertEqual(
            [(list(part.closed), part.value) for part in family.parts()],
            [
                ([2, 3], 4),
                ([1, 2, 3], 4),
                ([2, 3, 4], 4),
                ([1, 2, 3, 4], 4),
            ],
        )

    def test_stabilities(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        family = ExtensibleNumericalFamily()
        family.extend([closure(elements=[1, 2, 3]), closure(elements=[2, 3, 4])])
        self.assertEqual(
            [
                (list(stability.closed), stability.value)
                for stability in family.stabilities()
            ],
            [
                ([2, 3], 1.0),
                ([1, 2, 3], 0.5),
                ([2, 3, 4], 0.5),
                ([1, 2, 3, 4], 0.25),
            ],
        )

    def test_logarithmic_stabilities(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        family = ExtensibleNumericalFamily()
        family.extend(
            [
                closure(elements=[1, 2, 3]),
                closure(elements=[2, 3, 4]),
                closure(elements=[]),
            ],
        )
        self.assertEqual(
            [
                (list(stability.closed), round(stability.value, 3))
                for stability in family.logarithmic_stabilities()
            ],
            [
                ([], 1.0),
                ([2, 3], 0.5),
                ([1, 2, 3], 0.226),
                ([2, 3, 4], 0.226),
                ([1, 2, 3, 4], 0.075),
            ],
        )

    def test_probabilities(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        family = ExtensibleNumericalFamily()
        family.extend(
            [
                closure(elements=[1, 2, 3]),
                closure(elements=[2, 3, 4]),
                closure(),
            ],
        )
        self.assertEqual(
            [
                (list(probability.closed), probability.value)
                for probability in family.probabilities()
            ],
            [
                ([2, 3], 0.00390625),
                ([1, 2, 3], 0.00390625),
                ([2, 3, 4], 0.00390625),
                ([1, 2, 3, 4], 0.00390625),
                ([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 0.984375),
            ],
        )

    def test_information(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        family = ExtensibleNumericalFamily()
        family.extend(
            [
                closure(elements=[1, 2, 3]),
                closure(elements=[2, 3, 4]),
                closure(),
            ],
        )
        self.assertEqual(
            [
                (list(information.closed), round(information.value, 3))
                for information in family.information()
            ],
            [
                ([2, 3], 8.0),
                ([1, 2, 3], 8.0),
                ([2, 3, 4], 8.0),
                ([1, 2, 3, 4], 8.0),
                ([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 0.023),
            ],
        )

    def test_global_stability(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        family = ExtensibleNumericalFamily()
        family.extend(
            [
                closure(elements=[1, 2, 3]),
                closure(elements=[2, 3, 4]),
                closure(),
            ],
        )
        self.assertEqual(
            round(family.global_stability, 3),
            0.978,
        )

    def test_global_logarithmic_stability(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        family = ExtensibleNumericalFamily()
        family.extend(
            [
                closure(elements=[1, 2, 3]),
                closure(elements=[2, 3, 4]),
                closure(),
                closure(elements=[]),
            ],
        )
        self.assertEqual(
            round(family.global_logarithmic_stability, 3),
            0.587,
        )

    def test_global_entropy(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        family = ExtensibleNumericalFamily()
        family.extend(
            [
                closure(elements=[1, 2, 3]),
                closure(elements=[2, 3, 4]),
                closure(),
            ],
        )
        self.assertEqual(
            round(family.global_entropy, 3),
            0.147,
        )

    def test_global_information(self):
        closure = NumericalClosure(min_element=0, max_element=9)
        family = ExtensibleNumericalFamily()
        family.extend(
            [
                closure(elements=[1, 2, 3]),
                closure(elements=[2, 3, 4]),
                closure(),
            ],
        )
        self.assertEqual(
            round(family.global_information, 3),
            1.474,
        )
