"""
The :mod:`galactic.algebras.examples.connection` module.

It defines example classes for dealing with antitone finite Galois connection:

* :class:`IntegerConnection`;
* :class:`MultiplePolarity`;
* :class:`DivisorPolarity`;
* :class:`Multiple`;
* :class:`Divisors`.
"""

__all__ = (
    "IntegerConnection",
    "DivisorPolarity",
    "MultiplePolarity",
    "Divisors",
    "Multiple",
)

from ._integers import (
    DivisorPolarity,
    Divisors,
    IntegerConnection,
    Multiple,
    MultiplePolarity,
)
