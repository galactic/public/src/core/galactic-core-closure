""":mod:`galactic.algebras.connection` module."""

from __future__ import annotations

import itertools
from typing import TYPE_CHECKING, cast

from galactic.algebras.closure import AbstractClosure, ClosedEnumerableMixin
from galactic.algebras.connection import GaloisConnectionMixin
from galactic.algebras.lattice import Element
from galactic.algebras.set import FIFOSet, FrozenFIFOSet
from galactic.helpers.core import default_repr

if TYPE_CHECKING:
    from collections.abc import Iterable, Iterator


# pylint: disable=too-many-ancestors
class Multiple(ClosedEnumerableMixin[int], Element):  # type: ignore[misc]
    """
    It represents the multiple of some integers.

    Parameters
    ----------
    connection
        The Galois connection
    elements
        An optional iterable of int.

    Raises
    ------
    ValueError
        If an element is not in the domain.

    """

    __slots__ = ("_connection", "_elements")

    def __init__(
        self,
        connection: IntegerConnection,
        elements: Iterable[int] | None = None,
    ) -> None:
        self._connection = connection
        result = FIFOSet[int](connection.co_domain)
        elements = elements or []
        for element in elements:
            if element not in connection.domain:
                raise ValueError(f"{element} is not in the set")
            result -= {value for value in result if value % element != 0}
        self._elements = FIFOSet[int](result)

    def __repr__(self) -> str:
        return repr(list(self._elements))

    def __hash__(self) -> int:
        return hash(self._elements)

    def __len__(self) -> int:
        return len(self._elements)

    def __iter__(self) -> Iterator[int]:
        return iter(self._elements)

    def __contains__(self, value: object) -> bool:
        return value in self._elements

    @property
    def closure(self) -> AbstractClosure[Multiple, int]:
        """
        Get the closure operator.

        Returns
        -------
        AbstractClosure[Multiple, int]
            The closure operator.
        """
        return self._connection.closures[1]


# pylint: disable=too-many-ancestors
class Divisors(ClosedEnumerableMixin[int], Element):  # type: ignore[misc]
    """
    It represents the divisors of some integers.

    Parameters
    ----------
    connection
        An antitone Galois connection.
    elements
        An iterable of int.

    Raises
    ------
    ValueError
        If an element is not in the co-domain.

    """

    __slots__ = ("_connection", "_elements")

    def __init__(
        self,
        connection: IntegerConnection,
        elements: Iterable[int] | None = None,
    ) -> None:
        self._connection = connection
        result = FIFOSet[int](connection.domain)
        elements = elements or []
        for element in elements:
            if element not in connection.co_domain:
                raise ValueError(f"{element} is not in the set")
            result -= {value for value in result if element % value != 0}
        self._elements = FrozenFIFOSet[int](result)

    def __repr__(self) -> str:
        return repr(list(self._elements))

    def __hash__(self) -> int:
        return hash(self._elements)

    def __len__(self) -> int:
        return len(self._elements)

    def __iter__(self) -> Iterator[int]:
        return iter(self._elements)

    def __contains__(self, value: object) -> bool:
        return value in self._elements

    @property
    def closure(self) -> AbstractClosure[Divisors, int]:
        """
        Get the closure operator.

        Returns
        -------
        AbstractClosure[Divisors, int]
            The closure operator.
        """
        return self._connection.closures[0]


class MultiplePolarity:
    """
    It represents the polarity of multiple.

    Parameters
    ----------
    connection
        The antitone Galois connection
    """

    __slots__ = ("_connection",)

    def __init__(self, connection: IntegerConnection) -> None:
        self._connection = connection

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __call__(
        self,
        *others: Divisors,
        elements: Iterable[int] | None = None,
    ) -> Multiple:
        """
        Compute the correspondence of elements.

        Parameters
        ----------
        *others
            A sequence of divisors.
        elements
            An optional iterable of int.

        Returns
        -------
        Multiple
            The common multiple of elements.

        """
        return Multiple(self._connection, itertools.chain(*others, elements or []))


class DivisorPolarity:
    """
    It represents the polarity of divisors.
    """

    __slots__ = ("_connection",)

    def __init__(self, connection: IntegerConnection) -> None:
        self._connection = connection

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    def __call__(
        self,
        *others: Multiple,
        elements: Iterable[int] | None = None,
    ) -> Divisors:
        """
        Compute the correspondence of elements.

        Parameters
        ----------
        *others
            A sequence of Multiple.
        elements
            An optional iterable of int.

        Returns
        -------
        Divisors
            The common divisors of elements.

        """
        return Divisors(self._connection, itertools.chain(*others, elements or []))


class IntegerConnection(GaloisConnectionMixin[Divisors, int, Multiple, int]):
    """
    It represents an antitone Galois connection.

    Parameters
    ----------
    domain
        An iterable of int.
    co_domain
        An iterable of int.

    """

    __slots__ = (
        "_domain",
        "_co_domain",
        "_polarities",
    )

    # pylint: disable=too-few-public-methods
    def __init__(
        self,
        domain: Iterable[int],
        co_domain: Iterable[int],
    ) -> None:
        self._domain = FrozenFIFOSet[int](domain)
        self._co_domain = FrozenFIFOSet[int](co_domain)
        self._polarities = (MultiplePolarity(self), DivisorPolarity(self))
        super().__init__()

    @property
    def domain(self) -> FrozenFIFOSet[int]:
        """
        Get the domain.

        Returns
        -------
        FrozenFIFOSet[int]
            The domain.

        """
        return self._domain

    @property
    def co_domain(self) -> FrozenFIFOSet[int]:
        """
        Get the co-domain.

        Returns
        -------
        FrozenFIFOSet[int]
            The co-domain.

        """
        return self._co_domain

    @property
    def polarities(self) -> tuple[MultiplePolarity, DivisorPolarity]:
        """
        Get the polarities.

        Returns
        -------
        tuple[MultiplePolarity, DivisorPolarity]
            The couple of polarities.

        """
        return self._polarities
