"""Numerical closure example."""

# pylint: disable=wrong-import-position
from __future__ import annotations

from typing import TYPE_CHECKING, cast

from galactic.algebras.closure import (
    ClosedEnumerableMixin,
    ExtensibleMooreFamilyEnumerable,
)
from galactic.algebras.lattice import Element
from galactic.helpers.core import default_repr

if TYPE_CHECKING:
    from collections.abc import Iterable, Iterator


class NumericalClosed(  # pylint: disable=too-many-ancestors
    ClosedEnumerableMixin[int],
    Element,  # type: ignore[misc]
):
    """
    It represents finite closed sets of ints.

    It is defined by the minimal and maximal value.

    Parameters
    ----------
    closure
        The closure operator
    min_element
        An optional minimum element
    max_element
        An optional maximum element
    """

    __slots__ = ("_closure", "_length", "_min_element", "_max_element")

    _length: int
    _min_element: int | None
    _max_element: int | None

    def __init__(
        self,
        closure: NumericalClosure,
        min_element: int | None = None,
        max_element: int | None = None,
    ) -> None:
        self._closure = closure
        self._min_element = min_element
        self._max_element = max_element
        self._length = 0
        if self._min_element is not None and self._max_element is not None:
            self._length = self._max_element - self._min_element + 1
        else:
            self._length = 0

    def __repr__(self) -> str:
        if self._min_element is None or self._max_element is None:
            return "∅"
        return f"{self._min_element}..{self._max_element}"

    def __hash__(self) -> int:
        return hash((self._min_element, self._max_element))

    def __len__(self) -> int:
        return self._length

    def __iter__(self) -> Iterator[int]:
        if self._min_element is None or self._max_element is None:
            return iter([])
        return iter(range(self._min_element, self._max_element + 1))

    def __contains__(self, item: object) -> bool:
        if isinstance(item, int):
            return (
                self._min_element is not None
                and self._max_element is not None
                and self._min_element <= item <= self._max_element
            )
        return False

    @property
    def min_element(self) -> int | None:
        """
        Get the minimum element.

        Returns
        -------
        int | None
            The minimum element or None if  there is no minimum element.

        """
        return self._min_element

    @property
    def max_element(self) -> int | None:
        """
        Get the maximum element.

        Returns
        -------
        int | None
            The maximum element or None if there is no maximum element.

        """
        return self._max_element

    @property
    def closure(self) -> NumericalClosure:
        """
        Get the closure.

        Returns
        -------
        NumericalClosure
            The closure.

        """
        return self._closure


class NumericalClosure:
    """
    iT represents a simple numerical closure.

    Parameters
    ----------
    min_element
        The minimum element.
    max_element
        The maximum element.

    """

    __slots__ = ("_min_element", "_max_element")

    _min_element: int | None
    _max_element: int | None

    def __init__(
        self,
        min_element: int | None = None,
        max_element: int | None = None,
    ) -> None:
        if min_element is None or max_element is None or min_element > max_element:
            self._min_element = None
            self._max_element = None
        else:
            self._min_element = min_element
            self._max_element = max_element

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    # pylint: disable=too-many-branches
    def __call__(
        self,
        *others: NumericalClosed,
        elements: Iterable[int] | None = None,
    ) -> NumericalClosed:
        """
        Compute the closure of an iterable of elements.

        Parameters
        ----------
        *others
            A sequence of finite closed sets.
        elements
            An optional iterable of int.

        Returns
        -------
        NumericalClosed
            The new closed set.

        Raises
        ------
        ValueError
            If an element is not in the universe.
        ValueError
            If the closure is not correct.
        TypeError
            If an element is not of the correct class.

        """
        if self._min_element is None or self._max_element is None:
            return NumericalClosed(self)
        if elements is None and not others:
            return NumericalClosed(self, self._min_element, self._max_element)
        min_element = self._max_element
        max_element = self._min_element
        for element in elements or []:
            if self._min_element <= element <= self._max_element:
                min_element = min(min_element, element)
                max_element = max(max_element, element)
            else:
                raise ValueError(f"{element} is not in the universe")
        for other in others:
            if isinstance(other, NumericalClosed):
                if other.closure is self:
                    if other.min_element is not None:
                        min_element = min(min_element, other.min_element)
                    if other.max_element is not None:
                        max_element = max(max_element, other.max_element)
                else:
                    raise ValueError("The closure is not self")
            else:
                raise TypeError("The type of {other} is not correct")
        if min_element <= max_element:
            return NumericalClosed(self, min_element, max_element)
        return NumericalClosed(self)


# pylint: disable=too-few-public-methods,too-many-ancestors
class ExtensibleNumericalFamily(ExtensibleMooreFamilyEnumerable[NumericalClosed]):
    """
    It defines a Moore family on subset of integers.
    """
