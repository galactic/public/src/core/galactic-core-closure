"""
The :mod:`galactic.algebras.examples.closure` module.

It defines classes for testing closures.

* :class:`NumericalClosed` for representing closed sets of integers;
* :class:`NumericalClosure` for representing closure of sets of integers;
* :class:`ExtensibleNumericalFamily` for representing Moore family of closed sets of
  integers;
"""

__all__ = (
    "NumericalClosed",
    "NumericalClosure",
    "ExtensibleNumericalFamily",
)

from ._main import (
    ExtensibleNumericalFamily,
    NumericalClosed,
    NumericalClosure,
)
