from collections.abc import Iterable, Iterator

from galactic.algebras.closure import (
    ClosedEnumerableMixin,
    ExtensibleMooreFamilyEnumerable,
)
from galactic.algebras.lattice import Element

class NumericalClosed(
    ClosedEnumerableMixin[int],
    Element,  # type: ignore[misc]
):
    def __init__(
        self,
        closure: NumericalClosure,
        min_element: int | None = None,
        max_element: int | None = None,
    ) -> None: ...
    def __len__(self) -> int: ...
    def __iter__(self) -> Iterator[int]: ...
    def __contains__(self, item: object) -> bool: ...
    @property
    def min_element(self) -> int | None: ...
    @property
    def max_element(self) -> int | None: ...

class NumericalClosure:
    def __init__(
        self,
        min_element: int | None = None,
        max_element: int | None = None,
    ) -> None: ...
    def __call__(
        self,
        *closed: NumericalClosed,
        elements: Iterable[int] | None = None,
    ) -> NumericalClosed: ...

class ExtensibleNumericalFamily(ExtensibleMooreFamilyEnumerable[NumericalClosed]): ...
