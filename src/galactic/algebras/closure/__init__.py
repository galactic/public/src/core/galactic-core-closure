"""
The :mod:`galactic.algebras.closure` module.

It defines classes for creating closure operators, closed sets and Moore families.

.. rubric:: Closure operators

* :class:`AbstractClosure` for representing generic closure operators;

.. rubric:: Closed sets

* :class:`AbstractClosed` for representing generic closed sets;
* :class:`AbstractClosedEnumerable` for representing enumerable closed sets;
* :class:`ClosedMixin` for representing closed set mixins;
* :class:`ClosedEnumerableMixin` for representing enumerable closed set mixins;

.. rubric:: Moore family

* :class:`AbstractMooreFamily` for representing generic finite Moore families;
* :class:`AbstractMooreFamilyEnumerable` for representing generic finite
  Moore families of enumerable elements;
* :class:`MooreFamilyEnumerableMixin` for implementing some methods of finite
  Moore families of enumerable elements;
* :class:`FrozenMooreFamily` for representing frozen Moore families
* :class:`ExtensibleMooreFamily` for representing extensible Moore families
* :class:`FrozenMooreFamilyEnumerable`  for representing frozen Moore families
  of enumerable elements.
* :class:`ExtensibleMooreFamilyEnumerable` for representing extensible Moore families
  of enumerable elements.
"""

__all__ = (
    # Closure operators
    "AbstractClosure",
    # Closed sets
    "AbstractClosed",
    "AbstractClosedEnumerable",
    "ClosedMixin",
    "ClosedEnumerableMixin",
    # Moore families
    "AbstractMooreFamily",
    "AbstractMooreFamilyEnumerable",
    "MooreFamilyEnumerableMixin",
    "FrozenMooreFamily",
    "ExtensibleMooreFamily",
    "FrozenMooreFamilyEnumerable",
    "ExtensibleMooreFamilyEnumerable",
)

from ._abstract import (
    AbstractClosed,
    AbstractClosedEnumerable,
    AbstractClosure,
    AbstractMooreFamily,
    AbstractMooreFamilyEnumerable,
)
from ._closed import (
    ClosedEnumerableMixin,
    ClosedMixin,
)
from ._family import (
    ExtensibleMooreFamily,
    ExtensibleMooreFamilyEnumerable,
    FrozenMooreFamily,
    FrozenMooreFamilyEnumerable,
    MooreFamilyEnumerableMixin,
)
