"""Closure family module."""

# pylint: disable=too-many-lines, wrong-import-position
from __future__ import annotations

from math import log2
from typing import (
    TYPE_CHECKING,
    Any,
    Generic,
    TypeVar,
)

if TYPE_CHECKING:
    from collections.abc import Iterator


from galactic.algebras.lattice import (
    ExtensibleFiniteLattice,
    FrozenFiniteLattice,
)

from ._abstract import (
    AbstractClosed,
    AbstractClosedEnumerable,
    AbstractMooreFamilyEnumerable,
    Measure,
)

_C = TypeVar("_C", bound=AbstractClosed[Any])


class FrozenMooreFamily(
    FrozenFiniteLattice[_C],  # type: ignore[misc]
    Generic[_C],
):
    """
    It represents frozen finite Moore families.

    Notes
    -----
    See https://en.wikipedia.org/wiki/Closure_operator

    """

    __slots__ = ()


_E = TypeVar("_E", bound=AbstractClosedEnumerable[Any])


# pylint: disable=too-few-public-methods
class ExtensibleMooreFamily(
    ExtensibleFiniteLattice[_E],  # type: ignore[misc]
    FrozenMooreFamily[_E],
    Generic[_E],
):
    """
    It represents extensible finite Moore families.

    Notes
    -----
    See https://en.wikipedia.org/wiki/Closure_operator

    """

    __slots__ = ()


class MooreFamilyEnumerableMixin(Generic[_E]):
    """
    Mixin class for Moore families.
    """

    __slots__ = ()

    def parts(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> Iterator[Measure[_E, int]]:
        r"""
        Get an iterator on (:math:`A`, :math:`\kappa(A)`).

        :math:`\kappa(A)` is the number of parts of :math:`A` whose
        closure :math:`\phi` equal to :math:`A`.

        .. math::
            \kappa(A)=
            \left|\left\{
            \tilde A\subseteq A: \phi\left(\tilde A\right)=A
            \right\}\right|

        Yields
        ------
        Measure[_E, int]
            A couple (elements, #parts) :math:`\tilde A\subseteq A\subseteq X` whose
            closure is equal to the element.

        Examples
        --------
        >>> from galactic.algebras.examples.closure import ExtensibleNumericalFamily
        >>> from galactic.algebras.examples.closure import NumericalClosure
        >>> closure = NumericalClosure(min_element=0, max_element=5)
        >>> closure
        <galactic.algebras.examples.closure.NumericalClosure object at 0x...>
        >>> family = ExtensibleNumericalFamily()
        >>> family
        <galactic.algebras.examples.closure.ExtensibleNumericalFamily object at 0x...>
        >>> family.extend(
        ...     [
        ...         closure(elements=[1, 2, 3]),
        ...         closure(elements=[2, 3, 4]),
        ...         closure()
        ...     ]
        ... )
        >>> for part in family.parts():
        ...     print(part.closed, part.value)
        2..3 4
        1..3 4
        2..4 4
        1..4 4
        0..5 48

        """
        count: dict[_E, int] = {}
        for neighbourhood in self.cover.neighbourhoods(reverse=True):
            value = 1 << len(neighbourhood.element)
            for predecessor, predecessor_value in count.items():
                if predecessor <= neighbourhood.element:
                    value -= predecessor_value
            count[neighbourhood.element] = value
            yield Measure[_E, int](closed=neighbourhood.element, value=value)

    def stabilities(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> Iterator[Measure[_E, float]]:
        r"""
        Get an iterator on (:math:`A`, :math:`\sigma(A)`).

        :math:`\sigma(A)` is the stability of :math:`A`.

        .. math::
            \sigma(A)=\frac{\kappa(A)}{2^{|A|}}

        See Also
        --------
        :meth:`parts`

        Yields
        ------
        Measure[_E, float]
            A couple (elements, stability), an iterator on elements and their stability.

        Examples
        --------
        >>> from galactic.algebras.examples.closure import ExtensibleNumericalFamily
        >>> from galactic.algebras.examples.closure import NumericalClosure
        >>> closure = NumericalClosure(min_element=0, max_element=5)
        >>> closure
        <galactic.algebras.examples.closure.NumericalClosure object at 0x...>
        >>> family = ExtensibleNumericalFamily()
        >>> family
        <galactic.algebras.examples.closure.ExtensibleNumericalFamily object at 0x...>
        >>> family.extend(
        ...     [
        ...         closure(elements=[1, 2, 3]),
        ...         closure(elements=[2, 3, 4]),
        ...         closure(),
        ...     ]
        ... )
        >>> for stability in family.stabilities():
        ...     print(stability.closed, round(stability.value, 3))
        2..3 1.0
        1..3 0.5
        2..4 0.5
        1..4 0.25
        0..5 0.75

        """
        for part in self.parts():
            yield Measure[_E, float](
                closed=part.closed,
                value=part.value / (1 << len(part.closed)),
            )

    def logarithmic_stabilities(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> Iterator[Measure[_E, float]]:
        r"""
        Get an iterator on (:math:`A`, :math:`\lambda(A)`).

        :math:`\lambda(A)` is the normalized logarithmic stabilities
        of :math:`A` (the normalized logarithmic stability is always between 0
        and 1).

        * if :math:`\kappa(A)` is equal to :math:`2^{|A|}`, the normalized
          logarithmic stability is equal to 1.
        * if :math:`\kappa(A)` is equal to :math:`1`, the normalized
          logarithmic stability is equal to 0
        * the special case :math:`A=\emptyset`
          (:math:`2^{|A|}=\kappa(A)=1`) gives
          :math:`\lambda(\emptyset)=1` by convention.

        .. math::
            \begin{eqnarray}
            \lambda(A)
            &=&\frac{-\log_2\left(1-\sigma(A)+\frac{1}{2^{|A|}}\right)}{|A|}\\
            &=&\frac{-\log_2\left(\frac{2^{|A|}-\kappa(A)+1}{2^{|A|}}\right)}{|A|}\\
            &=&\frac{|A|-\log_2\left(2^{|A|}-\kappa(A)+1\right)}{|A|}\\
            &=&1-\frac{\log_2\left(2^{|A|}-\kappa(A)+1\right)}{|A|}\\
            \lambda(\emptyset)
            &=&1\\
            \end{eqnarray}

        See Also
        --------
        :meth:`parts`

        Notes
        -----
        Originally, the logarithmic stability was defined using

        .. math::
            \begin{eqnarray}
            -\log_2\left(1-\sigma(A)\right)
            &=&-\log_2\left(1-\frac{\kappa(A)}{2^{|A|}}\right)\\
            &=&|A|-\log_2\left(2^{|A|}-\kappa(A)\right)\\
            \end{eqnarray}

        but this formulae can give an infinity value, and it is not
        normalized between elements.

        References
        ----------
        `A. Buzmakov , S. Kuznetsov , A. Napoli , Scalable estimates of
        element stability, in: C. Glodeanu, M. Kaytoue, C. Sacarea (Eds.),
        Formal element Analysis, Lecture Notes in Computer Science, 8478,
        Springer International Publishing, 2014, pp. 157-172
        <https://doi.org/10.1007/978-3-319-07248-7_12>`_

        Yields
        ------
        Measure[_E, float]
            A couple (elements, normalized logarithmic stability).

        Examples
        --------
        >>> from galactic.algebras.examples.closure import ExtensibleNumericalFamily
        >>> from galactic.algebras.examples.closure import NumericalClosure
        >>> closure = NumericalClosure(min_element=0, max_element=5)
        >>> closure
        <galactic.algebras.examples.closure.NumericalClosure object at 0x...>
        >>> family = ExtensibleNumericalFamily()
        >>> family
        <galactic.algebras.examples.closure.ExtensibleNumericalFamily object at 0x...>
        >>> family.extend(
        ...     [
        ...         closure(elements=[1, 2, 3]),
        ...         closure(elements=[2, 3, 4]),
        ...         closure()
        ...     ]
        ... )
        >>> for stability in family.logarithmic_stabilities():
        ...     print(stability.closed, round(stability.value, 3))
        2..3 1.0
        1..3 0.226
        2..4 0.226
        1..4 0.075
        0..5 0.319

        """
        for part in self.parts():
            if part.closed:
                yield Measure[_E, float](
                    closed=part.closed,
                    value=1
                    - log2((1 << len(part.closed)) - part.value + 1) / len(part.closed),
                )
            else:
                yield Measure[_E, float](closed=part.closed, value=1.0)

    def probabilities(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> Iterator[Measure[_E, float]]:
        r"""
        Get an iterator on (:math:`A`, :math:`p(A)`).

        :math:`p(A)` is the probability of :math:`A`.

        .. math::
            p(A)=\frac{\kappa(A)}{2^n}

        See Also
        --------
        :meth:`parts`

        Yields
        ------
        Measure[_E, float]
            A couple (elements, probability).

        Examples
        --------
        >>> from galactic.algebras.examples.closure import ExtensibleNumericalFamily
        >>> from galactic.algebras.examples.closure import NumericalClosure
        >>> closure = NumericalClosure(min_element=0, max_element=5)
        >>> closure
        <galactic.algebras.examples.closure.NumericalClosure object at 0x...>
        >>> family = ExtensibleNumericalFamily()
        >>> family
        <galactic.algebras.examples.closure.ExtensibleNumericalFamily object at 0x...>
        >>> family.extend(
        ...     [
        ...         closure(elements=[1, 2, 3]),
        ...         closure(elements=[2, 3, 4]),
        ...         closure()
        ...     ]
        ... )
        >>> for probability in family.probabilities():
        ...     print(probability.closed, probability.value)
        2..3 0.0625
        1..3 0.0625
        2..4 0.0625
        1..4 0.0625
        0..5 0.75

        """
        for part in self.parts():
            yield Measure[_E, float](
                closed=part.closed,
                value=part.value / (1 << len(self.maximum)),
            )

    def information(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> Iterator[Measure[_E, float]]:
        r"""
        Get an iterator on elements (:math:`A`, :math:`I(A)`).

        :math:`I(A)` is the information of :math:`A`.

        .. math::
            I(A)=-\log_2(p(A)

        Yields
        ------
        Measure[_E, float]
            A couple (elements, information).

        Notes
        -----
        See
        `Information \
        <https://en.wikipedia.org/wiki/Entropy_(information_theory)#Introduction>`_.

        Examples
        --------
        >>> from galactic.algebras.examples.closure import ExtensibleNumericalFamily
        >>> from galactic.algebras.examples.closure import NumericalClosure
        >>> closure = NumericalClosure(min_element=0, max_element=5)
        >>> closure
        <galactic.algebras.examples.closure.NumericalClosure object at 0x...>
        >>> family = ExtensibleNumericalFamily()
        >>> family
        <galactic.algebras.examples.closure.ExtensibleNumericalFamily object at 0x...>
        >>> family.extend(
        ...     [
        ...         closure(elements=[1, 2, 3]),
        ...         closure(elements=[2, 3, 4]),
        ...         closure()
        ...     ]
        ... )
        >>> for information in family.information():
        ...     print(information.closed, information.value)
        2..3 4.0
        1..3 4.0
        2..4 4.0
        1..4 4.0
        0..5 0.4150374992788439

        """
        for part in self.parts():
            yield Measure[_E, float](
                closed=part.closed,
                value=len(self.maximum) - log2(part.value),
            )

    @property
    def global_stability(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> float:
        r"""
        Compute the global stability of the Moore family.

        .. math::
            \sigma(L)=\sum_{A\in L}p(A)\sigma(A)
            =\sum_{A\in L}\frac{\kappa(A)^2}{2^{|A|+n}}

        Returns
        -------
        float
            The global stability.

        Examples
        --------
        >>> from galactic.algebras.examples.closure import ExtensibleNumericalFamily
        >>> from galactic.algebras.examples.closure import NumericalClosure
        >>> closure = NumericalClosure(min_element=0, max_element=5)
        >>> closure
        <galactic.algebras.examples.closure.NumericalClosure object at 0x...>
        >>> family = ExtensibleNumericalFamily()
        >>> family
        <galactic.algebras.examples.closure.ExtensibleNumericalFamily object at 0x...>
        >>> family.extend(
        ...     [
        ...         closure(elements=[1, 2, 3]),
        ...         closure(elements=[2, 3, 4]),
        ...         closure()
        ...     ]
        ... )
        >>> round(family.global_stability, 3)
        0.703

        """
        return sum(
            part.value**2 / (1 << (len(part.closed) + len(self.maximum)))
            for part in self.parts()
        )

    @property
    def global_logarithmic_stability(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> float:
        r"""
        Compute the global normalized logarithmic stability of the Moore family.

        .. math::
            \lambda(L)=\sum_{A\in L}p(A)\lambda(A)

        Returns
        -------
        float
            The global normalized logarithmic stability.

        Examples
        --------
        >>> from galactic.algebras.examples.closure import ExtensibleNumericalFamily
        >>> from galactic.algebras.examples.closure import NumericalClosure
        >>> closure = NumericalClosure(min_element=0, max_element=5)
        >>> closure
        <galactic.algebras.examples.closure.NumericalClosure object at 0x...>
        >>> family = ExtensibleNumericalFamily()
        >>> family
        <galactic.algebras.examples.closure.ExtensibleNumericalFamily object at 0x...>
        >>> family.extend(
        ...     [
        ...         closure(elements=[1, 2, 3]),
        ...         closure(elements=[2, 3, 4]),
        ...         closure()
        ...     ]
        ... )
        >>> round(family.global_logarithmic_stability, 3)
        0.335

        """
        logarithmic_stability = 0.0
        for part in self.parts():
            if len(part.closed) != 0:
                value = log2((1 << len(part.closed)) - part.value + 1)
                logarithmic_stability += (
                    part.value
                    / (1 << len(self.maximum))
                    * (1 - value / len(part.closed))
                )
            else:
                logarithmic_stability += 1 / (1 << len(self.maximum))
        return logarithmic_stability

    @property
    def global_entropy(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> float:
        r"""
        Compute the global entropy of the Moore family.

        .. math::
            H(L)=\sum_{A\in L}p(A)I(A)=n-\frac{\sum_{A\in L}\kappa((
            A,B))\log_2(\kappa(A))}{2^n}

        Notes
        -----
        See
        `Information \
        <https://en.wikipedia.org/wiki/Entropy_(information_theory)#Introduction>`_.

        Returns
        -------
        float
            The global entropy.

        Examples
        --------
        >>> from galactic.algebras.examples.closure import ExtensibleNumericalFamily
        >>> from galactic.algebras.examples.closure import NumericalClosure
        >>> closure = NumericalClosure(min_element=0, max_element=5)
        >>> closure
        <galactic.algebras.examples.closure.NumericalClosure object at 0x...>
        >>> family = ExtensibleNumericalFamily()
        >>> family
        <galactic.algebras.examples.closure.ExtensibleNumericalFamily object at 0x...>
        >>> family.extend(
        ...     [
        ...         closure(elements=[1, 2, 3]),
        ...         closure(elements=[2, 3, 4]),
        ...         closure()
        ...     ]
        ... )
        >>> round(family.global_entropy, 3)
        1.311

        """
        return len(self.maximum) - sum(
            (part.value / (1 << len(self.maximum))) * log2(part.value)
            for part in self.parts()
        )

    @property
    def global_information(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> float:
        """
        Compute the global information of the Moore family.

        It is equal to the global entropy multiplied by the number of element defined by
        the closure operator.

        .. math::
            I(L)=n H(L)

        See Also
        --------
        See
        `Information \
        <https://en.wikipedia.org/wiki/Entropy_(information_theory)#Introduction>`_.

        Returns
        -------
        float
            The global information.

        Examples
        --------
        >>> from galactic.algebras.examples.closure import ExtensibleNumericalFamily
        >>> from galactic.algebras.examples.closure import NumericalClosure
        >>> closure = NumericalClosure(min_element=0, max_element=5)
        >>> closure
        <galactic.algebras.examples.closure.NumericalClosure object at 0x...>
        >>> family = ExtensibleNumericalFamily()
        >>> family
        <galactic.algebras.examples.closure.ExtensibleNumericalFamily object at 0x...>
        >>> family.extend(
        ...     [
        ...         closure(elements=[1, 2, 3]),
        ...         closure(elements=[2, 3, 4]),
        ...         closure()
        ...     ]
        ... )
        >>> round(family.global_information, 3)
        7.868

        """
        return self.global_entropy * len(self.maximum)


# noinspection PyTypeChecker
class FrozenMooreFamilyEnumerable(
    FrozenFiniteLattice[_E],  # type: ignore[misc]
    MooreFamilyEnumerableMixin[_E],
    Generic[_E],
):
    """
    It represents finite Moore families of enumerable closed sets.

    Notes
    -----
    See https://en.wikipedia.org/wiki/Closure_operator

    Examples
    --------
    >>> from galactic.algebras.examples.closure import ExtensibleNumericalFamily
    >>> from galactic.algebras.examples.closure import NumericalClosure
    >>> closure = NumericalClosure(0, 5)
    >>> closure
    <galactic.algebras.examples.closure.NumericalClosure object at 0x...>
    >>> family = ExtensibleNumericalFamily()
    >>> family
    <galactic.algebras.examples.closure.ExtensibleNumericalFamily object at 0x...>
    >>> list(family)
    []
    >>> family.extend(
    ...     [
    ...         closure(elements=[0, 1, 3]),
    ...         closure(elements=[2, 5])
    ...     ]
    ... )
    >>> list(family)
    [0..5, 0..3, 2..5, 2..3]
    >>> family.extend([closure(elements=[4, 5])])
    >>> list(family)
    [0..5, 0..3, 2..5, 2..3, 4..5, ∅]

    """

    __slots__ = ()


class ExtensibleMooreFamilyEnumerable(
    ExtensibleFiniteLattice[_E],  # type: ignore[misc]
    FrozenMooreFamilyEnumerable[_E],
):
    """
    It represents extensible finite Moore families of enumerable closed sets.

    Notes
    -----
    See https://en.wikipedia.org/wiki/Closure_operator

    """

    __slots__ = ()
