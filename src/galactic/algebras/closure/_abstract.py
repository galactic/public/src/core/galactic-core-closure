"""Abstract closure module."""

from __future__ import annotations

from abc import abstractmethod
from collections.abc import Container, Iterable, Iterator, Sized
from dataclasses import dataclass
from typing import Any, Generic, Protocol, TypeVar, cast, runtime_checkable

from typing_extensions import Self

from galactic.algebras.lattice import AbstractFiniteLattice, Element
from galactic.helpers.core import default_repr

_T = TypeVar("_T")


@runtime_checkable
class AbstractClosed(Element, Container[_T], Protocol[_T]):  # type: ignore[misc]
    """
    It represents abstract closed sets.
    """

    @abstractmethod
    def __le__(self, other: object) -> bool:
        return NotImplemented

    @abstractmethod
    def __lt__(self, other: object) -> bool:
        return NotImplemented

    @abstractmethod
    def __gt__(self, other: object) -> bool:
        return NotImplemented

    @abstractmethod
    def __ge__(self, other: object) -> bool:
        return NotImplemented

    @property
    @abstractmethod
    def closure(self) -> AbstractClosure[Self, _T]:
        """
        Get the closure operator.

        Returns
        -------
        AbstractClosure[Self, _T]
            The closure operator.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


# pylint: disable=too-many-ancestors
@runtime_checkable
class AbstractClosedEnumerable(  # type: ignore[misc]
    AbstractClosed[_T],
    Sized,
    Iterable[_T],
    Protocol[_T],
):
    """
    It represents abstract enumerable closed sets.
    """

    @property
    def support(self) -> float:
        """
        Get the closed set support.

        Raises
        ------
        NotImplementedError
            This method should be implemented in concrete classes.

        """
        raise NotImplementedError

    def equivalence(self, element: _T) -> Iterator[_T]:
        """
        Compute equivalences of the element.

        Parameters
        ----------
        element
            The element whose equivalences are requested

        Raises
        ------
        NotImplementedError
            This method should be implemented in concrete classes.

        """
        raise NotImplementedError

    def subsumption(self, element: _T) -> Iterator[_T]:
        """
        Compute subsumed elements of the element.

        Parameters
        ----------
        element
            The element whose subsumed elements are requested

        Raises
        ------
        NotImplementedError
            This method should be implemented in concrete classes.

        """
        raise NotImplementedError

    def supsumption(self, element: _T) -> Iterator[_T]:
        """
        Compute supsumed elements of the element.

        Notes
        -----
        *supsumption* is a neologism to designate the inverse relation of
        subsumption.

        Parameters
        ----------
        element
            The element whose supsumed elements are requested

        Raises
        ------
        NotImplementedError
            This method should be implemented in concrete classes.

        """
        raise NotImplementedError


_C = TypeVar("_C", bound=AbstractClosed[Any])


# pylint: disable=too-few-public-methods
@runtime_checkable
class AbstractClosure(Protocol[_C, _T]):  # type: ignore[misc]
    r"""
    It represents a closure operator.

    The Greek letter :math:`\phi` is often used to name a closure operator.

    Notes
    -----
    See https://en.wikipedia.org/wiki/Closure_operator

    """

    @abstractmethod
    def __call__(
        self,
        *others: _C,
        elements: Iterable[_T] | None = None,
    ) -> _C:
        """
        Call this closure on an iterable of elements.

        Parameters
        ----------
        *others
            A sequence of closed sets of the same closure operator.
        elements
            An optional iterable of elements.

        Returns
        -------
        _C
            The closed representation of the ``elements`` and of the ``other``
            closed sets.

        Notes
        -----
        If elements is None and others is empty, the closed universe is returned.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


@runtime_checkable
class AbstractMooreFamily(  # type: ignore[misc]
    AbstractFiniteLattice[_C],  # type: ignore[misc]
    Protocol[_C],
):  # pylint: disable=too-many-ancestors
    """
    It represents finite Moore families of closed elements.

    Notes
    -----
    See https://en.wikipedia.org/wiki/Closure_operator

    """


_E = TypeVar("_E", bound=AbstractClosedEnumerable[Any])
_V = TypeVar("_V")


@dataclass(frozen=True)
class Measure(Generic[_E, _V]):
    """
    It associates a measure to a finite enumerable closed set.

    It is a dataclass containing two fields:

    * :attr:`closed` representing the enumerable closed set;
    * :attr:`value` representing the value of the measure.
    """

    closed: _E
    value: _V

    def __repr__(self) -> str:
        return cast(str, default_repr(self))


@runtime_checkable
class AbstractMooreFamilyEnumerable(  # type: ignore[misc]
    AbstractFiniteLattice[_E],  # type: ignore[misc]
    Protocol[_E],
):  # pylint: disable=too-many-ancestors
    """
    It represents finite Moore families of closed enumerable elements.

    Notes
    -----
    See https://en.wikipedia.org/wiki/Closure_operator

    """

    @abstractmethod
    def parts(self) -> Iterator[Measure[_E, int]]:
        r"""
        Get an iterator on (:math:`A`, :math:`\kappa(A)`).

        :math:`\kappa(A)` is the number of parts of :math:`A` whose
        closure :math:`\phi` equal to :math:`A`.

        .. math::
            \kappa(A)=
            \left|\left\{
            \tilde A\subseteq A: \phi\left(\tilde A\right)=A
            \right\}\right|

        Yields
        ------
        Measure[_E, int]
            A couple (elements, #parts) :math:`\tilde A\subseteq A\subseteq X` whose
            closure is equal to the element.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def stabilities(self) -> Iterator[Measure[_E, float]]:
        r"""
        Get an iterator on (:math:`A`, :math:`\sigma(A)`).

        :math:`\sigma(A)` is the stability of :math:`A`.

        .. math::
            \sigma(A)=\frac{\kappa(A)}{2^{|A|}}

        See Also
        --------
        :meth:`parts`

        Yields
        ------
        Measure[_E, float]
            A couple (elements, stability), an iterator on elements and their stability.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def logarithmic_stabilities(self) -> Iterator[Measure[_E, float]]:
        r"""
        Get an iterator on (:math:`A`, :math:`\lambda(A)`).

        :math:`\lambda(A)` is the normalized logarithmic stabilities
        of :math:`A` (the normalized logarithmic stability is always between 0
        and 1).

        * if :math:`\kappa(A)` is equal to :math:`2^{|A|}`, the normalized
          logarithmic stability is equal to 1.
        * if :math:`\kappa(A)` is equal to :math:`1`, the normalized
          logarithmic stability is equal to 0
        * the special case :math:`A=\emptyset`
          (:math:`2^{|A|}=\kappa(A)=1`) gives
          :math:`\lambda(\emptyset)=1` by convention.

        .. math::
            \begin{eqnarray}
            \lambda(A)
            &=&\frac{-\log_2\left(1-\sigma(A)+\frac{1}{2^{|A|}}\right)}{|A|}\\
            &=&\frac{-\log_2\left(\frac{2^{|A|}-\kappa(A)+1}{2^{|A|}}\right)}{|A|}\\
            &=&\frac{|A|-\log_2\left(2^{|A|}-\kappa(A)+1\right)}{|A|}\\
            &=&1-\frac{\log_2\left(2^{|A|}-\kappa(A)+1\right)}{|A|}\\
            \lambda(\emptyset)
            &=&1\\
            \end{eqnarray}

        See Also
        --------
        :meth:`parts`

        Notes
        -----
        Originally, the logarithmic stability was defined using

        .. math::
            \begin{eqnarray}
            -\log_2\left(1-\sigma(A)\right)
            &=&-\log_2\left(1-\frac{\kappa(A)}{2^{|A|}}\right)\\
            &=&|A|-\log_2\left(2^{|A|}-\kappa(A)\right)\\
            \end{eqnarray}

        but this formulae can give an infinity value, and it is not
        normalized between elements.

        References
        ----------
        `A. Buzmakov , S. Kuznetsov , A. Napoli , Scalable estimates of
        element stability, in: C. Glodeanu, M. Kaytoue, C. Sacarea (Eds.),
        Formal element Analysis, Lecture Notes in Computer Science, 8478,
        Springer International Publishing, 2014, pp. 157-172
        <https://doi.org/10.1007/978-3-319-07248-7_12>`_

        Yields
        ------
        Measure[_E, float]
            A couple (elements, normalized logarithmic stability).

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def probabilities(self) -> Iterator[Measure[_E, float]]:
        r"""
        Get an iterator on (:math:`A`, :math:`p(A)`).

        :math:`p(A)` is the probability of :math:`A`.

        .. math::
            p(A)=\frac{\kappa(A)}{2^n}

        See Also
        --------
        :meth:`parts`

        Yields
        ------
        Measure[_E, float]
            A couple (elements, probability).

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @abstractmethod
    def information(self) -> Iterator[Measure[_E, float]]:
        r"""
        Get an iterator on elements (:math:`A`, :math:`I(A)`).

        :math:`I(A)` is the information of :math:`A`.

        .. math::
            I(A)=-\log_2(p(A)

        Yields
        ------
        Measure[_E, float]
            A couple (elements, information).

        Notes
        -----
        See
        `Information \
        <https://en.wikipedia.org/wiki/Entropy_(information_theory)#Introduction>`_.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def global_stability(self) -> float:
        r"""
        Compute the global stability of the Moore family.

        .. math::
            \sigma(L)=\sum_{A\in L}p(A)\sigma(A)
            =\sum_{A\in L}\frac{\kappa(A)^2}{2^{|A|+n}}

        Returns
        -------
        float
            The global stability.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def global_logarithmic_stability(self) -> float:
        r"""
        Compute the global normalized logarithmic stability of the Moore family.

        .. math::
            \lambda(L)=\sum_{A\in L}p(A)\lambda(A)

        Returns
        -------
        float
            The global normalized logarithmic stability.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def global_entropy(self) -> float:
        r"""
        Compute the global entropy of the Moore family.

        .. math::
            H(L)=\sum_{A\in L}p(A)I(A)=n-\frac{\sum_{A\in L}\kappa((
            A,B))\log_2(\kappa(A))}{2^n}

        Notes
        -----
        See
        `Information \
        <https://en.wikipedia.org/wiki/Entropy_(information_theory)#Introduction>`_.

        Returns
        -------
        float
            The global entropy.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def global_information(self) -> float:
        """
        Compute the global information of the Moore family.

        It is equal to the global entropy multiplied by the number of element defined by
        the closure operator.

        .. math::
            I(L)=n H(L)

        See Also
        --------
        See
        `Information \
        <https://en.wikipedia.org/wiki/Entropy_(information_theory)#Introduction>`_.

        Returns
        -------
        float
            The global information.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError
