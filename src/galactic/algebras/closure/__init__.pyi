from abc import abstractmethod
from collections.abc import (
    Container,
    Iterable,
    Iterator,
    Sized,
)
from dataclasses import dataclass
from typing import (
    Any,
    Generic,
    Protocol,
    TypeVar,
    runtime_checkable,
)

from typing_extensions import Self

from galactic.algebras.lattice import (
    AbstractFiniteLattice,
    Element,
    ExtensibleFiniteLattice,
    FrozenFiniteLattice,
)

_T = TypeVar("_T")
_C = TypeVar("_C", bound=AbstractClosed[Any])
_E = TypeVar("_E", bound=AbstractClosedEnumerable[Any])
_V = TypeVar("_V")

@runtime_checkable
class AbstractClosed(Element, Container[_T], Protocol[_T]):  # type: ignore[misc]
    @abstractmethod
    def __le__(self, other: object) -> bool: ...
    @abstractmethod
    def __lt__(self, other: object) -> bool: ...
    @abstractmethod
    def __gt__(self, other: object) -> bool: ...
    @abstractmethod
    def __ge__(self, other: object) -> bool: ...
    @property
    @abstractmethod
    def closure(self) -> AbstractClosure[Self, _T]: ...

@runtime_checkable
class AbstractClosedEnumerable(  # type: ignore[misc]
    AbstractClosed[_T],
    Sized,
    Iterable[_T],
    Protocol[_T],
):
    @property
    @abstractmethod
    def support(self) -> float: ...
    @abstractmethod
    def equivalence(self, element: _T) -> Iterator[_T]: ...
    @abstractmethod
    def subsumption(self, element: _T) -> Iterator[_T]: ...
    @abstractmethod
    def supsumption(self, element: _T) -> Iterator[_T]: ...

@runtime_checkable
class AbstractClosure(Protocol[_C, _T]):  # type: ignore[misc]
    @abstractmethod
    def __call__(
        self,
        *others: _C,
        elements: Iterable[_T] | None = None,
    ) -> _C: ...

@runtime_checkable
class AbstractMooreFamily(  # type: ignore[misc]
    AbstractFiniteLattice[_C],  # type: ignore[misc]
    Protocol[_C],
): ...

@runtime_checkable
class AbstractMooreFamilyEnumerable(  # type: ignore[misc]
    AbstractFiniteLattice[_E],  # type: ignore[misc]
    Protocol[_E],
):
    @abstractmethod
    def parts(self) -> Iterator[Measure[_E, int]]: ...
    @abstractmethod
    def stabilities(self) -> Iterator[Measure[_E, float]]: ...
    @abstractmethod
    def logarithmic_stabilities(self) -> Iterator[Measure[_E, float]]: ...
    @abstractmethod
    def probabilities(self) -> Iterator[Measure[_E, float]]: ...
    @abstractmethod
    def information(self) -> Iterator[Measure[_E, float]]: ...
    @property
    @abstractmethod
    def global_stability(self) -> float: ...
    @property
    @abstractmethod
    def global_logarithmic_stability(self) -> float: ...
    @property
    @abstractmethod
    def global_entropy(self) -> float: ...
    @property
    @abstractmethod
    def global_information(self: AbstractMooreFamilyEnumerable[_E]) -> float: ...

class MooreFamilyEnumerableMixin(Generic[_E]):
    def parts(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> Iterator[Measure[_E, int]]: ...
    def stabilities(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> Iterator[Measure[_E, float]]: ...
    def logarithmic_stabilities(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> Iterator[Measure[_E, float]]: ...
    def probabilities(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> Iterator[Measure[_E, float]]: ...
    def information(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> Iterator[Measure[_E, float]]: ...
    @property
    def global_stability(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> float: ...
    @property
    def global_logarithmic_stability(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> float: ...
    @property
    def global_entropy(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> float: ...
    @property
    def global_information(
        self: AbstractMooreFamilyEnumerable[_E],
    ) -> float: ...

# pylint: disable=too-few-public-methods
class ClosedMixin(Generic[_T]):
    def __or__(self: AbstractClosed[_T], other: object) -> AbstractClosed[_T]: ...

class ClosedEnumerableMixin(ClosedMixin[_T], Generic[_T]):
    def __le__(self: AbstractClosedEnumerable[_T], other: object) -> bool: ...
    def __lt__(self: AbstractClosedEnumerable[_T], other: object) -> bool: ...
    def __gt__(self: AbstractClosedEnumerable[_T], other: object) -> bool: ...
    def __ge__(self: AbstractClosedEnumerable[_T], other: object) -> bool: ...
    def __and__(
        self: AbstractClosedEnumerable[_T],
        other: AbstractClosedEnumerable[_T],
    ) -> AbstractClosedEnumerable[_T]: ...
    @property
    def support(self) -> float: ...
    def equivalence(self, element: _T) -> Iterator[_T]: ...
    def subsumption(self, element: _T) -> Iterator[_T]: ...
    def supsumption(self, element: _T) -> Iterator[_T]: ...

class FrozenMooreFamily(
    FrozenFiniteLattice[_C],  # type: ignore[misc]
    Generic[_C],
): ...
class ExtensibleMooreFamily(
    ExtensibleFiniteLattice[_E],  # type: ignore[misc]
    FrozenMooreFamily[_E],
    Generic[_E],
): ...

@dataclass(frozen=True)
class Measure(Generic[_E, _V]):
    closed: _E
    value: _V

class FrozenMooreFamilyEnumerable(
    FrozenFiniteLattice[_E],  # type: ignore[misc]
    Generic[_E],
):
    def parts(self) -> Iterator[Measure[_E, int]]: ...
    def stabilities(self) -> Iterator[Measure[_E, float]]: ...
    def logarithmic_stabilities(self) -> Iterator[Measure[_E, float]]: ...
    def probabilities(self) -> Iterator[Measure[_E, float]]: ...
    def information(self) -> Iterator[Measure[_E, float]]: ...
    @property
    def global_stability(self) -> float: ...
    @property
    def global_logarithmic_stability(self) -> float: ...
    @property
    def global_entropy(self) -> float: ...
    @property
    def global_information(self) -> float: ...

class ExtensibleMooreFamilyEnumerable(
    ExtensibleFiniteLattice[_E],  # type: ignore[misc]
    FrozenMooreFamilyEnumerable[_E],
): ...
