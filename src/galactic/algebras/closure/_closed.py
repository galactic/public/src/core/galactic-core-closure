"""Closure main module."""

# pylint: disable=wrong-import-position
from __future__ import annotations

# pylint: disable=unused-import
from typing import TYPE_CHECKING, Generic, TypeVar, cast

from galactic.helpers.core import default_repr

from ._abstract import AbstractClosed, AbstractClosedEnumerable

if TYPE_CHECKING:
    from collections.abc import Iterator

_T = TypeVar("_T")


# pylint: disable=too-few-public-methods
class ClosedMixin(Generic[_T]):
    """
    Closed set mixin.
    """

    __slots__ = ()

    def __repr__(self: AbstractClosed[_T]) -> str:
        return cast(str, default_repr(self))

    def __or__(self: AbstractClosed[_T], other: object) -> object:
        if isinstance(other, self.__class__) and other.closure is self.closure:
            return self.closure(self, other)
        return NotImplemented


class ClosedEnumerableMixin(ClosedMixin[_T], Generic[_T]):
    """
    It represents enumerable closed sets mixins.
    """

    __slots__ = ()

    def __eq__(self: AbstractClosedEnumerable[_T], other: object) -> bool:
        if isinstance(other, self.__class__):
            return len(self) == len(other) and self.__le__(other)
        return NotImplemented

    def __le__(self: AbstractClosedEnumerable[_T], other: object) -> bool:
        if isinstance(other, self.__class__):
            if len(self) > len(other):
                return False
            return all(not element not in other for element in self)
        return NotImplemented

    def __lt__(self: AbstractClosedEnumerable[_T], other: object) -> bool:
        if isinstance(other, self.__class__):
            return len(self) < len(other) and self.__le__(other)
        return NotImplemented

    def __gt__(self: AbstractClosedEnumerable[_T], other: object) -> bool:
        if isinstance(other, self.__class__):
            return len(self) > len(other) and self.__ge__(other)
        return NotImplemented

    def __ge__(self: AbstractClosedEnumerable[_T], other: object) -> bool:
        if isinstance(other, self.__class__):
            if len(self) < len(other):
                return False
            return all(not element not in self for element in other)
        return NotImplemented

    def __and__(self: AbstractClosedEnumerable[_T], other: object) -> object:
        if isinstance(other, self.__class__) and other.closure is self.closure:
            return self.closure(
                elements=(element for element in other if element in self),
            )
        return NotImplemented

    @property
    def support(self: AbstractClosedEnumerable[_T]) -> float:
        """
        Get the closed set support.

        Returns
        -------
        float
            The closed set support.

        """
        length = len(self.closure())
        if length == 0:
            return 1.0
        return len(self) / length

    def equivalence(
        self: AbstractClosedEnumerable[_T],
        element: _T,
    ) -> Iterator[_T]:
        """
        Compute equivalences of the element.

        Parameters
        ----------
        element
            The element whose equivalences are requested

        Returns
        -------
        Iterator[_T]
            An iterator over the equivalence elements of element

        """
        closed = self.closure(elements=[element])
        return (member for member in self if self.closure(elements=[member]) == closed)

    def subsumption(
        self: AbstractClosedEnumerable[_T],
        element: _T,
    ) -> Iterator[_T]:
        """
        Compute subsumed elements of the element.

        Parameters
        ----------
        element
            The element whose subsumed elements are requested

        Returns
        -------
        Iterator[_T]
            An iterator over the subsumed elements of element

        """
        closed = self.closure(elements=[element])
        return (member for member in self if self.closure(elements=[member]) < closed)

    def supsumption(
        self: AbstractClosedEnumerable[_T],
        element: _T,
    ) -> Iterator[_T]:
        """
        Compute supsumed elements of the element.

        Notes
        -----
        *supsumption* is a neologism to designate the inverse relation of
        subsumption.

        Parameters
        ----------
        element
            The element whose *supsumed* elements are requested

        Returns
        -------
        Iterator[_T]
            An iterator over the *supsumed* elements of element

        """
        closed = self.closure(elements=[element])
        return (member for member in self if self.closure(elements=[member]) > closed)


if __name__ == "__main__":
    import doctest

    doctest.testmod()
