from abc import abstractmethod
from collections.abc import (
    Iterable,
)
from typing import (
    Any,
    Generic,
    Protocol,
    TypeVar,
    runtime_checkable,
)

from galactic.algebras.closure import (
    AbstractClosed,
    AbstractClosure,
)

_S = TypeVar("_S")
_T = TypeVar("_T")
_C = TypeVar("_C", bound=AbstractClosed[Any])
_D = TypeVar("_D", bound=AbstractClosed[Any])

@runtime_checkable
class AbstractPolarity(Protocol[_C, _S, _D]):  # type: ignore[misc]
    @abstractmethod
    def __call__(
        self,
        *others: _C,
        elements: Iterable[_S] | None = None,
    ) -> _D: ...

@runtime_checkable
class AbstractGaloisConnection(Protocol[_C, _S, _D, _T]):
    @property
    @abstractmethod
    def polarities(
        self,
    ) -> tuple[AbstractPolarity[_C, _S, _D], AbstractPolarity[_D, _T, _C]]: ...
    @property
    @abstractmethod
    def closures(
        self,
    ) -> tuple[AbstractClosure[_C, _S], AbstractClosure[_D, _T]]: ...

class GaloisConnectionMixin(Generic[_C, _S, _D, _T]):
    @property
    def closures(self) -> tuple[
        AbstractClosure[_C, _S],
        AbstractClosure[_D, _T],
    ]: ...
