"""Abstract connection module."""

from __future__ import annotations

from abc import abstractmethod
from typing import TYPE_CHECKING, Any, Protocol, TypeVar, runtime_checkable

from galactic.algebras.closure import (
    AbstractClosed,
    AbstractClosure,
)

if TYPE_CHECKING:
    from collections.abc import Iterable

_S = TypeVar("_S")
_T = TypeVar("_T")
_C = TypeVar("_C", bound=AbstractClosed[Any])
_D = TypeVar("_D", bound=AbstractClosed[Any])


# pylint: disable=too-few-public-methods
@runtime_checkable
class AbstractPolarity(Protocol[_C, _S, _D]):  # type: ignore[misc]
    """
    It represents polarities for antitone Galois connections.

    * the type ``_C`` represents the type of the closed of the left part of
      the antitone Galois connection;
    * the type ``_S`` represents the type of the elements of the left part of
      the antitone Galois connection;
    * the type ``_D`` represents the type of the closed of the right part of
      the antitone Galois connection;

    Note
    ----
    See https://en.wikipedia.org/wiki/Galois_connection#Antitone_Galois_connection.

    """

    @abstractmethod
    def __call__(
        self,
        *others: _C,
        elements: Iterable[_S] | None = None,
    ) -> _D:
        """
        Call this instance.

        Parameters
        ----------
        *others
            A sequence of AbstractClosed.
        elements
            An optional iterable of elements.

        Returns
        -------
        _D
            The correspondence of the arguments.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError


@runtime_checkable
class AbstractGaloisConnection(Protocol[_C, _S, _D, _T]):
    # noinspection PyUnresolvedReferences
    """
    It represents antitone Galois connections.

    * the type ``_C`` represents the type of the closed of the left part of
      the antitone Galois connection;
    * the type ``_S`` represents the type of the elements of the left part of
      the antitone Galois connection;
    * the type ``_D`` represents the type of the closed of the right part of
      the antitone Galois connection;
    * the type ``_T`` represents the type of the elements of the right part of
      the antitone Galois connection.

    Note
    ----
    See https://en.wikipedia.org/wiki/Galois_connection.

    Examples
    --------
    >>> from galactic.algebras.examples.connection import IntegerConnection
    >>> connection = IntegerConnection(
    ...     [2, 3, 4, 5, 6],
    ...     [10, 12, 18, 24, 36, 48, 30, 60],
    ... )
    >>> polarities = connection.polarities
    >>> list(polarities[0](elements=[2, 3, 4]))
    [12, 24, 36, 48, 60]
    >>> list(polarities[1](polarities[0](elements=[2, 3, 4])))
    [2, 3, 4, 6]
    >>> list(polarities[1]([30]))
    [2, 3, 5, 6]
    >>> list(polarities[0](polarities[1](elements=[30])))
    [30, 60]
    >>> list(polarities[1]([10]))
    [2, 5]
    >>> list(polarities[0](polarities[1](elements=[10])))
    [10, 30, 60]
    >>> closures = connection.closures
    >>> list(closures[0](elements=[2, 3]))
    [2, 3, 6]
    >>> list(closures[1](elements=[30]))
    [30, 60]

    """

    @property
    @abstractmethod
    def polarities(
        self,
    ) -> tuple[AbstractPolarity[_C, _S, _D], AbstractPolarity[_D, _T, _C]]:
        """
        Get the polarities.

        Returns
        -------
        tuple[AbstractPolarity[_C, _S, _D], AbstractPolarity[_D, _T, _S]]
            The polarities.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError

    @property
    @abstractmethod
    def closures(
        self,
    ) -> tuple[AbstractClosure[_C, _S], AbstractClosure[_D, _T]]:
        """
        Get the closures.

        Returns
        -------
        tuple[AbstractClosure[_C, _S], AbstractClosure[_D, _T]]
            The closures.

        Raises
        ------
        NotImplementedError

        """
        raise NotImplementedError
