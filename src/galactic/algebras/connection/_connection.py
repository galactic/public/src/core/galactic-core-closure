""":mod:`galactic.algebras.connection` module."""

from __future__ import annotations

from typing import (
    Any,
    Generic,
    TypeVar,
)

from compose import compose

from galactic.algebras.closure import AbstractClosed, AbstractClosure

from ._abstract import AbstractGaloisConnection

_S = TypeVar("_S")
_T = TypeVar("_T")
_C = TypeVar("_C", bound=AbstractClosed[Any])
_D = TypeVar("_D", bound=AbstractClosed[Any])


# pylint: disable=too-few-public-methods
class GaloisConnectionMixin(Generic[_C, _S, _D, _T]):
    # noinspection PyTypeChecker,PyUnresolvedReferences
    """
    It represents antitone Galois connections.

    * the type ``_C`` represents the type of the closed of the left part of
      the antitone Galois connection;
    * the type ``_S`` represents the type of the elements of the left part of
      the antitone Galois connection;
    * the type ``_D`` represents the type of the closed of the right part of
      the antitone Galois connection;
    * the type ``_T`` represents the type of the elements of the right part of
      the antitone Galois connection.

    Note
    ----
    See https://en.wikipedia.org/wiki/Galois_connection.

    Examples
    --------
    >>> from galactic.algebras.examples.connection import IntegerConnection
    >>> connection = IntegerConnection(
    ...     [2, 3, 4, 5, 6],
    ...     [10, 12, 18, 24, 36, 48, 30, 60],
    ... )
    >>> polarities = connection.polarities
    >>> list(polarities[0](elements=[2, 3, 4]))
    [12, 24, 36, 48, 60]
    >>> list(polarities[1](polarities[0](elements=[2, 3, 4])))
    [2, 3, 4, 6]
    >>> list(polarities[1]([30]))
    [2, 3, 5, 6]
    >>> list(polarities[0](polarities[1](elements=[30])))
    [30, 60]
    >>> list(polarities[1]([10]))
    [2, 5]
    >>> list(polarities[0](polarities[1](elements=[10])))
    [10, 30, 60]
    >>> closures = connection.closures
    >>> list(closures[0](elements=[2, 3]))
    [2, 3, 6]
    >>> list(closures[1](elements=[30]))
    [30, 60]

    """

    __slots__ = ("_closures",)

    _closures: tuple[AbstractClosure[_C, _S], AbstractClosure[_D, _T]]

    def __init__(self: AbstractGaloisConnection[_C, _S, _D, _T]) -> None:
        polarities = self.polarities
        # noinspection PyTypeChecker
        self._closures = (  # type: ignore[attr-defined]
            # polarities[1] ∘ polarities[0] (β∘α)
            compose(polarities[1], polarities[0]),
            # polarities[0] ∘ polarities[1] (α∘β)
            compose(polarities[0], polarities[1]),
        )

    @property
    def closures(self) -> tuple[AbstractClosure[_C, _S], AbstractClosure[_D, _T]]:
        """
        Get the closures.

        Returns
        -------
        tuple[AbstractClosure[_C, _S], AbstractClosure[_D, _T]]
            The closures.

        """
        return self._closures
