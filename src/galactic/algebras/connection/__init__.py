"""
The :mod:`galactic.algebras.connection` module.

It defines several classes for dealing with antitone Galois connection defined
on powersets:

* :class:`AbstractPolarity`;
* :class:`AbstractGaloisConnection`;
* :class:`GaloisConnectionMixin`;
"""

__all__ = (
    "AbstractPolarity",
    "AbstractGaloisConnection",
    "GaloisConnectionMixin",
)

from ._abstract import AbstractGaloisConnection, AbstractPolarity
from ._connection import GaloisConnectionMixin
