=========================
**GALACTIC** core closure
=========================

*galactic-core-closure* [#logo]_ [#logoclosure]_ is the package for studying
closures.

**GALACTIC** stands for
**GA**\ lois
**LA**\ ttices,
**C**\ oncept
**T**\ heory,
**I**\ mplicational systems and
**C**\ losures.

..  toctree::
    :maxdepth: 1
    :caption: Getting started

    install/index.rst

..  toctree::
    :maxdepth: 1
    :caption: Learning

    notebooks

..  toctree::
    :maxdepth: 1
    :caption: Reference

    api/closure/index.rst
    api/connection/index.rst
    api/examples/index.rst

..  toctree::
    :maxdepth: 1
    :caption: Release Notes

    release-notes.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. [#logo]
        The **GALACTIC** icon has been constructed using icons present in the
        `free star wars icons set <http://sensibleworld.com/news/free-star-wars-icons>`_
        designed by `Sensible World <http://www.iconarchive.com/artist/sensibleworld.html>`_.
        The product or characters depicted in these icons
        are © by Disney / Lucasfilm.

        It's a tribute to the french mathematician
        `Évariste Galois <https://en.wikipedia.org/wiki/Évariste_Galois>`_ who
        died at age 20 in a duel.
        In France, Concept Lattices are also called *Galois Lattices*.

.. [#logoclosure]
        The *galactic-core-closure* icon has been constructed using the main
        **GALACTIC** icon and the `Closed icon <https://uxwing.com/closed-icon/>`_.
