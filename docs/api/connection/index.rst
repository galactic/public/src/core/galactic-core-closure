Antitone Galois connection
==========================

.. automodule:: galactic.algebras.connection
    :members:
    :inherited-members:
