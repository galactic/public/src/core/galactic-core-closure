Closures
========

.. automodule:: galactic.algebras.closure
    :members:
    :inherited-members:
