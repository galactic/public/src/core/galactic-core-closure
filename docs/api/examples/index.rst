Closure examples
================

.. automodule:: galactic.algebras.examples.closure
    :members:
    :inherited-members:

Connection examples
===================

.. automodule:: galactic.algebras.examples.connection
    :members:
    :inherited-members:
